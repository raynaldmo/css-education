#### Float based layouts
* franklin-running-club.html
  * Uses flex box for laying out top portion to get equal height
  * No grid system used
  
* franklin-running-club-grid.html
  * Uses grid system
  
* Floating and flex box are incompatible. Need to use one or the
other.

* No easy way to get even height columns with floats ?  